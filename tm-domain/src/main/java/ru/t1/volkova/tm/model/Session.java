package ru.t1.volkova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_session")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Session extends AbstractUserOwnedModel {

    @NotNull
    @Column(nullable = false)
    private Date date = new Date();

    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = null;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Session session = (Session) o;
        return session.getId().equals(((Session) o).getId()) && session.getUser().getId().equals(((Session) o).getUser().getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDate(), getRole());
    }

}
