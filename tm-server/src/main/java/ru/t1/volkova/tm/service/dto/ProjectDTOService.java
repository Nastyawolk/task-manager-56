package ru.t1.volkova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.volkova.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.volkova.tm.api.service.dto.IProjectDTOService;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.field.*;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

@Service
public final class ProjectDTOService extends AbstractUserOwnedDTOService<ProjectDTO, IProjectDTORepository>
        implements IProjectDTOService {

    @Override
    @NotNull
    public IProjectDTORepository getRepository() {
        return context.getBean(IProjectDTORepository.class);
    }

    @Nullable
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        @NotNull final IProjectDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ProjectDTO project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        @NotNull final IProjectDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        @NotNull final IProjectDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ProjectDTO project = findOneById(userId, id);
        if (status != null) project.setStatus(status);
        @NotNull final IProjectDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ProjectDTO project = findOneByIndex(userId, index);
        if (status != null) project.setStatus(status);
        @NotNull final IProjectDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            findOneById(userId, id);
            entityManager.getTransaction().begin();
            repository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @NotNull final IProjectDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            findOneByIndex(userId, index);
            repository.removeOneByIndex(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final ProjectDTO project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @NotNull final IProjectDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final ProjectDTO project = repository.findOneByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        } finally {
            entityManager.close();
        }
    }

    @SuppressWarnings("unchecked")
    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId,
                                    @NotNull final Comparator comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final List<ProjectDTO> projects = repository.findAll(userId, comparator);
            if (projects == null) throw new ProjectNotFoundException();
            return projects;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final List<ProjectDTO> projects = repository.findAll(userId);
            if (projects == null) throw new ProjectNotFoundException();
            return projects;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final List<ProjectDTO> projects = repository.findAll(userId);
            if (projects == null) throw new ProjectNotFoundException();
            return repository.getSize(userId);
        } finally {
            entityManager.close();
        }
    }

}
