package ru.t1.volkova.tm.api.repository.model;

import ru.t1.volkova.tm.model.Project;

public interface IProjectRepository extends IAbstractUserOwnedRepository<Project> {

}
