package ru.t1.volkova.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE

}
