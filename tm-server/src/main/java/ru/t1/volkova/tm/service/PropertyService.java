package ru.t1.volkova.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.volkova.tm.api.service.IDatabaseProperty;
import ru.t1.volkova.tm.api.service.IPropertyService;

import java.util.Properties;

@Service
public final class PropertyService implements IPropertyService, IDatabaseProperty {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String SERVER_HOST = "server.host";

    @NotNull
    public static final String SERVER_PORT = "server.port";

    @NotNull
    private static final String SESSION_KEY = "session.key";

    @NotNull
    private static final String SESSION_TIME_OUT = "session.timeout";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "25456";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "356585985";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String DATABASE_USERNAME = "database.username";

    @NotNull
    public static final String DATABASE_PASSWORD = "database.password";

    @NotNull
    public static final String DATABASE_URL = "database.url";

    @NotNull
    private static final String DATABASE_DRIVER_PROPERTY = "database.driver";

    @NotNull
    private static final String DATABASE_DIALECT = "database.dialect";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_SHOW_SQL = "database.show_sql";

    @NotNull
    private static final String DATABASE_USE_SECOND_LVL_CACHE = "database.cache.use_second_level_cache";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE = "database.cache.use_query_cache";

    @NotNull
    private static final String DATABASE_USE_MINIMAL_PUTS = "database.cache.use_minimal_puts";

    @NotNull
    private static final String DATABASE_REGION_PREFIX = "database.cache.region_prefix";

    @NotNull
    private static final String DATABASE_PROVIDER_CFG_FILE_RES_PATH = "database.cache.provider_configuration_file_resource_path";

    @NotNull
    private static final String DATABASE_REGION_FACTORY_CLASS = "database.cache.region.factory_class";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private static final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @Override
    @NotNull
    public String getServerHost() {
        return getStringValue(SERVER_HOST);
    }

    @Override
    @NotNull
    public Integer getServerPort() {
        return getIntegerValue(SERVER_PORT, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public String getSessionKey() {
        return getStringValue(SESSION_KEY);
    }

    @Override
    @NotNull
    public Integer getSessionTiomeOut() {
        return getIntegerValue(SESSION_TIME_OUT, EMPTY_VALUE);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @Override
    public @NotNull String getDatabasePassword() {
        return getStringValue(DATABASE_PASSWORD);
    }

    @Override
    public @NotNull String getDatabaseUrl() {
        return getStringValue(DATABASE_URL);
    }

    @Override
    public @NotNull String getDatabaseUsername() {
        return getStringValue(DATABASE_USERNAME);
    }

    @Override
    public @NotNull String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER_PROPERTY);
    }

    @Override
    public @NotNull String getDatabaseDialect() {
        return getStringValue(DATABASE_DIALECT);
    }

    @Override
    public @NotNull String getDatabaseHbm2ddlAuto() {
        return getStringValue(DATABASE_HBM2DDL_AUTO);
    }

    @Override
    public @NotNull String getDatabaseShowSql() {
        return getStringValue(DATABASE_SHOW_SQL);
    }

    @Override
    public @NotNull String getDatabaseUseSecondLvlCache() {
        return getStringValue(DATABASE_USE_SECOND_LVL_CACHE);
    }

    @Override
    public @NotNull String getDatabaseUseQueryCache() {
        return getStringValue(DATABASE_USE_QUERY_CACHE);
    }

    @Override
    public @NotNull String getDatabaseUseMinimalPuts() {
        return getStringValue(DATABASE_USE_MINIMAL_PUTS);
    }

    @Override
    public @NotNull String getDatabaseRegionPrefix() {
        return getStringValue(DATABASE_REGION_PREFIX);
    }

    @Override
    public @NotNull String getDatabaseProviderCfgFileResPath() {
        return getStringValue(DATABASE_PROVIDER_CFG_FILE_RES_PATH);
    }

    @Override
    public @NotNull String getDatabaseRegionFactoryClass() {
        return getStringValue(DATABASE_REGION_FACTORY_CLASS);
    }

}
