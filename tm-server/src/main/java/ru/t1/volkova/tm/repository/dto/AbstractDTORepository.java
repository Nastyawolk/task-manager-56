package ru.t1.volkova.tm.repository.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.volkova.tm.api.repository.dto.IAbstractDTORepository;
import ru.t1.volkova.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;

@Getter
@Repository
@Scope("prototype")
@NoArgsConstructor
public abstract class AbstractDTORepository<E extends AbstractModelDTO> implements IAbstractDTORepository<E> {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @Override
    public void add(@NotNull final E entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(@NotNull final E entity) {
        entityManager.merge(entity);
    }

}
