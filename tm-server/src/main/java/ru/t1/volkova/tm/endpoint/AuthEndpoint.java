package ru.t1.volkova.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.t1.volkova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.volkova.tm.api.service.IAuthService;
import ru.t1.volkova.tm.dto.request.user.UserLoginRequest;
import ru.t1.volkova.tm.dto.request.user.UserLogoutRequest;
import ru.t1.volkova.tm.dto.request.user.UserProfileRequest;
import ru.t1.volkova.tm.dto.response.user.UserLoginResponse;
import ru.t1.volkova.tm.dto.response.user.UserLogoutResponse;
import ru.t1.volkova.tm.dto.response.user.UserProfileResponse;
import ru.t1.volkova.tm.dto.model.SessionDTO;
import ru.t1.volkova.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.volkova.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndPoint implements IAuthEndpoint {

    @Override
    @NotNull
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) throws SQLException {
        @NotNull final IAuthService authService = getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @Override
    @NotNull
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) throws Exception {
        final SessionDTO session = check(request);
        getAuthService().invalidate(session);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserProfileRequest request
    ) throws Exception {
        @Nullable SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user;
        user = getUserService().findOneById(userId);
        return new UserProfileResponse(user);
    }

}
