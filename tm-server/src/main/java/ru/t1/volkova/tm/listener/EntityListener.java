package ru.t1.volkova.tm.listener;

import lombok.NoArgsConstructor;
import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.log.OperationEvent;
import ru.t1.volkova.tm.log.OperationType;

@NoArgsConstructor
public class EntityListener implements PostInsertEventListener,
        PostDeleteEventListener, PostUpdateEventListener {

    @Nullable
    private JmsLoggerProducer jmsLoggerProducer;

    public EntityListener(@NotNull final JmsLoggerProducer jmsLoggerProducer) {
        this.jmsLoggerProducer = jmsLoggerProducer;
    }

    @Override
    public void onPostDelete(@NotNull final PostDeleteEvent postDeleteEvent) {
        log(OperationType.DELETE, postDeleteEvent.getEntity());
    }

    @Override
    public void onPostInsert(@NotNull final PostInsertEvent postInsertEvent) {
        log(OperationType.INSERT, postInsertEvent.getEntity());
    }

    @Override
    public void onPostUpdate(@NotNull final PostUpdateEvent postUpdateEvent) {
        log(OperationType.UPDATE, postUpdateEvent.getEntity());
    }

    public boolean requiresPostCommitHanding(@NotNull final EntityPersister entityPersister) {
        return false;
    }

    public void log(
            @NotNull final OperationType operationType,
            @NotNull final Object entity
    ) {
        System.out.println(operationType + ":" + entity.hashCode());
        if (jmsLoggerProducer == null) return;
        jmsLoggerProducer.send(new OperationEvent(operationType, entity));
    }

}
