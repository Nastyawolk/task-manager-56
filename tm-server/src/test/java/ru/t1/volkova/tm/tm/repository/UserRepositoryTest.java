package ru.t1.volkova.tm.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.dto.IUserDTORepository;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.dto.model.UserDTO;
import ru.t1.volkova.tm.tm.migration.AbstractSchemeTest;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryTest extends AbstractSchemeTest  {

    private static final int NUMBER_OF_ENTRIES = 4;

    @NotNull
    private static final List<UserDTO> userList = new ArrayList<>();

    @NotNull
    private static IUserDTORepository getUserDTORepository() {
        return context.getBean(IUserDTORepository.class);
    }

    @BeforeClass
    public static void initRepository() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        createUsers();
    }

    private static void createUsers()  {
        @NotNull final IUserDTORepository userRepository = getUserDTORepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
                @NotNull UserDTO user = new UserDTO();
                user.setLogin("user" + i);
                user.setEmail("user@" + i + ".ru");
                user.setRole(Role.USUAL);
                userRepository.add(user);
                userList.add(user);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testAddUser() {
        @NotNull final UserDTO newUser = new UserDTO();
        @NotNull final IUserDTORepository userRepository = getUserDTORepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            int expectedNumberOfEntries = userRepository.getSize() + 1;
            entityManager.getTransaction().begin();
            userRepository.add(newUser);
            entityManager.getTransaction().commit();
            Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindAll() {
        @NotNull final IUserDTORepository userRepository = getUserDTORepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            @Nullable final List<UserDTO> userList = userRepository.findAll();
            Assert.assertNotEquals(0, userList.size());
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindOneById() {
        @NotNull final IUserDTORepository userRepository = getUserDTORepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            @NotNull final UserDTO expected = userList.get(1);
            Assert.assertEquals(expected, userRepository.findOneById(expected.getId()));
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final IUserDTORepository userRepository = getUserDTORepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            Assert.assertNull(userRepository.findOneById("NotExcitingId"));
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testRemoveAll() {
        @NotNull final IUserDTORepository userRepository = getUserDTORepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.clear();
            entityManager.getTransaction().commit();
            Assert.assertEquals(0, userRepository.getSize());
            createUsers();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @Test
    public void testGetSize() {
        @NotNull final IUserDTORepository userRepository = getUserDTORepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            Assert.assertNotEquals(0, userRepository.getSize());
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindByLogin() {
        @NotNull final IUserDTORepository userRepository = getUserDTORepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            Assert.assertEquals(userList.get(0), userRepository.findOneByLogin("user1"));
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindByLoginNegative() {
        @NotNull final IUserDTORepository userRepository = getUserDTORepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            Assert.assertNull(userRepository.findOneByLogin("user-test"));
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindByEmail() {
        @NotNull final IUserDTORepository userRepository = getUserDTORepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            Assert.assertEquals(userList.get(1), userRepository.findOneByEmail(userList.get(1).getEmail()));
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindByEmailNegative() {
        @NotNull final IUserDTORepository userRepository = getUserDTORepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            Assert.assertNull(userRepository.findOneByEmail("test@ru"));
        } finally {
            entityManager.close();
        }
    }

}
