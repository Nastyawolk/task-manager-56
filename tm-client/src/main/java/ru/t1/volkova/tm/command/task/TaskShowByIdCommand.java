package ru.t1.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.task.TaskGetByIdRequest;
import ru.t1.volkova.tm.dto.model.TaskDTO;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.sql.SQLException;

@Component
public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Show task by id.";

    @NotNull
    private static final String NAME = "task-show-by-id";

    @Override
    public void execute() throws SQLException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(getToken());
        request.setId(id);
        @Nullable final TaskDTO task = getTaskEndpoint().getTaskById(request).getTask();
        showTask(task);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
