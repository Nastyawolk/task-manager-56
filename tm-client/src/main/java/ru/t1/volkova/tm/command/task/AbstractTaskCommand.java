package ru.t1.volkova.tm.command.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.volkova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.volkova.tm.command.AbstractCommand;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import java.util.List;

@Component
@Getter
@Setter
public abstract class AbstractTaskCommand extends AbstractCommand {

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @NotNull
    @Autowired
    public ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    public IAuthEndpoint authEndpoint;

    protected void renderTasks(@Nullable final List<TaskDTO> tasks) {
        if (tasks == null) return;
        int index = 1;
        for (@Nullable final TaskDTO task : tasks) {
            if (task == null) continue;
            String print = index++ + ". " +
                    task.getName() + " | " +
                    task.getId() + " | " +
                    task.getStatus();
            System.out.println(print);
        }
    }

    protected void showTask(@Nullable final TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
