package ru.t1.volkova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.system.ServerAboutRequest;
import ru.t1.volkova.tm.dto.response.system.ServerAboutResponse;

@Component
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    private static final String DESCRIPTION = "Show developer info.";

    @NotNull
    private static final String NAME = "about";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final ServerAboutResponse response = getSystemEndpoint().getAbout(new ServerAboutRequest());
        System.out.println("name: " + response.getName());
        System.out.println("e-mail: " + response.getEmail());
    }

    @Override
    public @NotNull String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
