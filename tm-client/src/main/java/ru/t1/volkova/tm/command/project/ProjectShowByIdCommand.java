package ru.t1.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.project.ProjectGetByIdRequest;
import ru.t1.volkova.tm.dto.model.ProjectDTO;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.sql.SQLException;

@Component
public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Show project by id.";

    @NotNull
    private static final String NAME = "project-show-by-id";

    @Override
    public void execute() throws SQLException {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(getToken());
        request.setId(id);
        @Nullable final ProjectDTO project = getProjectEndpoint().getProjectById(request).getProject();
        showProject(project);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
