package ru.t1.volkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.user.UserUnlockRequest;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.sql.SQLException;

@Component
public final class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "Unlock user";

    @NotNull
    private static final String NAME = "user-unlock";

    @Override
    public void execute() throws SQLException {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().unlockUser(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
