package ru.t1.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.project.ProjectClearRequest;

import java.sql.SQLException;

@Component
public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Remove all projects.";

    @NotNull
    private static final String NAME = "project-clear";

    @Override
    public void execute() throws SQLException {
        System.out.println("[PROJECTS CLEAR]");
        getProjectEndpoint().clearProject(new ProjectClearRequest(getToken()));
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
