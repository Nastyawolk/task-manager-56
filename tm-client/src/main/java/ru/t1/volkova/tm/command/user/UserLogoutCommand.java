package ru.t1.volkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.user.UserLogoutRequest;

@Component
public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "Logout current user";

    @NotNull
    private static final String NAME = "logout";

    @Override
    public void execute() throws Exception {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        request.setToken(getToken());
        getAuthEndpoint().logout(request);
        setToken(null);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
