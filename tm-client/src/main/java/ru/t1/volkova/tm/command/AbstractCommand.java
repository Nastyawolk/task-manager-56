package ru.t1.volkova.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.api.model.ICommand;
import ru.t1.volkova.tm.api.service.ITokenService;

@Getter
@Setter
@Component
public abstract class AbstractCommand implements ICommand {

    @NotNull
    @Autowired
    ITokenService tokenService;

    @Nullable
    protected String getToken() {
        return getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        getTokenService().setToken(token);
    }

    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
