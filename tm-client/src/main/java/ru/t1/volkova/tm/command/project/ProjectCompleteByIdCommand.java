package ru.t1.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.sql.SQLException;

@Component
public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Complete project by id.";

    @NotNull
    private static final String NAME = "project-complete-by-id";

    @Override
    public void execute() throws SQLException {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatus(Status.COMPLETED);
        getProjectEndpoint().changeProjectStatusById(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
