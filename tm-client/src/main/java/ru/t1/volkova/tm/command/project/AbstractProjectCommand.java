package ru.t1.volkova.tm.command.project;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.volkova.tm.command.AbstractCommand;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

import java.util.List;

@Getter
@Setter
@Component
public abstract class AbstractProjectCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Autowired
    public IProjectEndpoint projectEndpoint;

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

    protected void renderProjects(@Nullable final List<ProjectDTO> projects) {
        if (projects == null) return;
        int index = 1;
        for (@Nullable final ProjectDTO project : projects) {
            if (project == null) continue;
            String print = index++ + ". " +
                    project.getName() + " | " +
                    project.getId() + " | " +
                    project.getStatus();
            System.out.println(print);
        }
    }

}
