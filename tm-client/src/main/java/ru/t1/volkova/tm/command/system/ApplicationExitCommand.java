package ru.t1.volkova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.api.service.ILoggerService;

@Component
public final class ApplicationExitCommand extends AbstractSystemCommand {

    @Nullable
    private static final String ARGUMENT = null;

    @NotNull
    private static final String DESCRIPTION = "Close application.";

    @NotNull
    private static final String NAME = "exit";

    @Override
    public void execute() {
        @NotNull final ILoggerService loggerService = getLoggerService();
        loggerService.info("** EXIT TASK MANAGER **");
        System.exit(0);
    }

    @Override
    public @Nullable String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
