package ru.t1.volkova.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.api.repository.ICommandRepository;
import ru.t1.volkova.tm.api.service.*;
import ru.t1.volkova.tm.command.AbstractCommand;
import ru.t1.volkova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.volkova.tm.exception.system.CommandNotSupportedException;
import ru.t1.volkova.tm.util.SystemUtil;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@Getter
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.volkova.tm.command";

    @Nullable
    @Autowired
    private AbstractCommand[] abstractCommands;

    @NotNull
    @Autowired
    @Getter
    private ICommandRepository commandRepository;

    @NotNull
    @Autowired
    @Getter
    private ICommandService commandService;

    @NotNull
    @Autowired
    @Getter
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    @Getter
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    @Getter
    private ITokenService tokenService;

    @NotNull
    @Autowired
    @Getter
    private FileScanner fileScanner;

    @NotNull
    @Autowired
    private ApplicationContext context;

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void registry(@Nullable final AbstractCommand[] commands) {
        for (@Nullable final AbstractCommand command : commands) {
            if (command == null) return;
            commandService.add(command);
        }
    }

    private void prepareStartup() {
        initPID();
        registry(abstractCommands);
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    public void start(@Nullable final String[] args) throws Exception {
        prepareStartup();
        processArguments(args);
        while (true) {
            try {
                System.out.println("ENTER COMMAND");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void processArguments(@Nullable final String[] args) throws Exception {
        if (args == null || args.length == 0) {
            return;
        }
        processArgument(args[0]);
        exit();
    }

    private void processArgument(@Nullable final String arg) throws Exception {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) throws Exception {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, final boolean checkRoles) throws Exception {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    public void exit() {
        System.exit(0);
    }

}
