package ru.t1.volkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.volkova.tm.util.TerminalUtil;

@Component
public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "Update profile of current user";

    @NotNull
    private static final String NAME = "update-user-profile";

    @Override
    public void execute() throws Exception {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken());
        request.setLastName(lastName);
        request.setFirstName(firstName);
        request.setMiddleName(middleName);
        getUserEndpoint().updateUserProfile(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
