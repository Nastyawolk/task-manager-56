package ru.t1.volkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.user.UserProfileRequest;
import ru.t1.volkova.tm.dto.model.UserDTO;

@Component
public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "View profile of current user";

    @NotNull
    private static final String NAME = "view-user-profile";

    @Override
    public void execute() throws Exception {
        @NotNull final UserProfileRequest request = new UserProfileRequest(getToken());
        @Nullable final UserDTO user = getAuthEndpoint().profile(request).getUser();
        if (user == null) return;
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
