package ru.t1.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.project.ProjectChangeStatusByIndexRequest;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.sql.SQLException;
import java.util.Arrays;

@Component
public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Change project status by index.";

    @NotNull
    private static final String NAME = "project-change-status-by-index";

    @Override
    public void execute() throws SQLException {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(getToken());
        request.setStatus(status);
        request.setIndex(index);
        getProjectEndpoint().changeProjectStatusByIndex(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
