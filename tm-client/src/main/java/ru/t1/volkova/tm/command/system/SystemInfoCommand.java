package ru.t1.volkova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.util.FormatUtil;

@Component
public final class SystemInfoCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-i";

    @NotNull
    private static final String DESCRIPTION = "Show system information.";

    @NotNull
    private static final String NAME = "info";

    @Override
    public void execute() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + FormatUtil.formatBytes(availableProcessors));
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCHeck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCHeck ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + FormatUtil.formatBytes(totalMemory));
        final long usageMemory = totalMemory - freeMemory;
        System.out.println("Usage memory: " + FormatUtil.formatBytes(usageMemory));
    }

    @Override
    public @NotNull String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
