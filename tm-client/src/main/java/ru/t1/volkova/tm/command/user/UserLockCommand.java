package ru.t1.volkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.user.UserLockRequest;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.sql.SQLException;

@Component
public final class UserLockCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "Lock user";

    @NotNull
    private static final String NAME = "user-lock";

    @Override
    public void execute() throws SQLException {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRequest request = new UserLockRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().lockUser(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
