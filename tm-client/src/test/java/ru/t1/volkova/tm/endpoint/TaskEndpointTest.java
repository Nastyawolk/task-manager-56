package ru.t1.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.volkova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.volkova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.volkova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.volkova.tm.dto.request.project.ProjectListRequest;
import ru.t1.volkova.tm.dto.request.task.*;
import ru.t1.volkova.tm.dto.request.user.UserLoginRequest;
import ru.t1.volkova.tm.dto.response.project.ProjectListResponse;
import ru.t1.volkova.tm.dto.response.task.*;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.marker.SoapCategory;
import ru.t1.volkova.tm.dto.model.ProjectDTO;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TaskEndpointTest {

    private static final int NUMBER_OF_ENTRIES = 5;

    @NotNull
    private static final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstanse();

    @NotNull
    private static final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstanse();

    @NotNull
    private static final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstanse();

    @Nullable
    private static String token;

    @NotNull
    private final static List<TaskDTO> taskList = new ArrayList<>();

    @NotNull
    private final static List<ProjectDTO> projectList = new ArrayList<>();

    private static int taskSize;

    private static int projectSize;

    private static String projectBindID;

    private static String taskBindID;

    @BeforeClass
    public static void initTest2() throws Exception {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest();
        requestLogin.setLogin("admin");
        requestLogin.setPassword("admin");
        token = authEndpoint.login(requestLogin).getToken();
        createTasks();
    }

    private static void createTasks() throws Exception {
        @NotNull final TaskListRequest requestListTasks = new TaskListRequest(token);
        @NotNull final TaskListResponse responseListTasks = taskEndpoint.listTask(requestListTasks);

        @NotNull final ProjectListRequest requestListProjects = new ProjectListRequest(token);
        @NotNull final ProjectListResponse responseListProjects = projectEndpoint.listProject(requestListProjects);

        if (responseListTasks.getTasks() != null && responseListProjects.getProjects() != null) {
            taskList.addAll(responseListTasks.getTasks());
            projectList.addAll(responseListProjects.getProjects());
        } else {
            @NotNull final ProjectListRequest requestProjectList = new ProjectListRequest(token);
            @NotNull final ProjectListResponse responseProjectList = projectEndpoint.listProject(requestProjectList);
            if (responseProjectList.getProjects() != null) {
                projectList.addAll(responseProjectList.getProjects());
            }

            for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
                @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(token);
                requestCreate.setName("Ctask" + i);
                requestCreate.setDescription("description" + i);
                @NotNull final TaskCreateResponse responseCreate = taskEndpoint.createTask(requestCreate);
                taskList.add(responseCreate.getTask());
            }
        }

        taskSize = taskList.size();
        projectSize = projectList.size();

        @NotNull final Random random = new Random();
        projectBindID = projectList.get(random.nextInt(projectSize)).getId();
        taskBindID = taskList.get(random.nextInt(taskSize)).getId();
    }

    @Test
    @Category(SoapCategory.class)
    public void testBindTaskToProject() throws Exception {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(token);
        request.setProjectId(projectBindID);
        request.setTaskId(taskBindID);
        taskEndpoint.bindTaskToProject(request);

        @NotNull final TaskListByProjectIdRequest requestList = new TaskListByProjectIdRequest(token);
        requestList.setProjectId(projectBindID);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.getTaskByProjectId(requestList).getTasks();
        if (tasks == null) {
            return;
        }
        Assert.assertNotEquals(tasks.size(), 0);
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeTaskStatusById() throws SQLException {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = taskList.get(random.nextInt(taskSize)).getId();
        @NotNull final String statusValue = "COMPLETED";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setId(id);
        request.setStatus(status);
        @NotNull final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(request);
        if (response.getTask() == null) return;
        Assert.assertEquals(status, response.getTask().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeTaskStatusByIndex() throws SQLException {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final TaskDTO task = taskList.get(random.nextInt(taskSize));
        @NotNull final Integer index = taskList.indexOf(task);
        @NotNull final String statusValue = "COMPLETED";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setIndex(index);
        request.setStatus(status);
        @NotNull final TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndex(request);
        if (response.getTask() == null) return;
        Assert.assertEquals(status, response.getTask().getStatus());
    }


    @Test
    @Category(SoapCategory.class)
    public void testCompleteTaskById() throws SQLException {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = taskList.get(random.nextInt(taskSize)).getId();
        @NotNull final String statusValue = "COMPLETED";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setId(id);
        request.setStatus(status);
        @NotNull final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(request);
        if (response.getTask() == null) return;
        Assert.assertEquals(status, response.getTask().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCompleteTaskByIndex() throws SQLException {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final TaskDTO task = taskList.get(random.nextInt(taskSize));
        @NotNull final Integer index = taskList.indexOf(task);
        @NotNull final String statusValue = "COMPLETED";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setIndex(index);
        request.setStatus(status);
        @NotNull final TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndex(request);
        if (response.getTask() == null) return;
        Assert.assertEquals(status, response.getTask().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCreateTask() throws SQLException {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(token);
        @NotNull final String taskName = "new task";
        @NotNull final String description = "new description";
        request.setName(taskName);
        request.setDescription(description);
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(request);
        Assert.assertNotNull(response);
        if (response.getTask() == null) return;
        Assert.assertEquals(taskName, response.getTask().getName());
        Assert.assertEquals(description, response.getTask().getDescription());
        taskList.add(response.getTask());
        taskSize = taskList.size();
    }

    @Test
    @Category(SoapCategory.class)
    public void testListTask() throws Exception {
        @NotNull final TaskListRequest requestList = new TaskListRequest(token);
        @NotNull final TaskListResponse responseList = taskEndpoint.listTask(requestList);
        if (responseList.getTasks() == null) return;
        Assert.assertEquals(taskSize, responseList.getTasks().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetTaskById() throws Exception {
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = taskList.get(random.nextInt(taskSize)).getId();
        request.setId(id);
        @NotNull final TaskGetByIdResponse response = taskEndpoint.getTaskById(request);
        if (response.getTask() == null) return;
        Assert.assertEquals(id, response.getTask().getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetTaskByIndex() throws Exception {
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(token);
        @NotNull final TaskListResponse responseList = taskEndpoint.listTask(new TaskListRequest(token));
        @NotNull final Random random = new Random();
        if (responseList.getTasks() == null) return;
        @NotNull final TaskDTO task = responseList.getTasks().get(random.nextInt(taskSize));
        final int index = responseList.getTasks().indexOf(task);
        request.setIndex(index);
        @NotNull final TaskGetByIndexResponse response = taskEndpoint.getTaskByIndex(request);
        if (response.getTask() == null) return;
        Assert.assertNotNull(response.getTask());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetTaskByProjectId() throws SQLException {
        @NotNull final Random random = new Random();
        @NotNull final String projectID = projectList.get(random.nextInt(projectSize)).getId();
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(token);
        request.setProjectId(projectID);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.getTaskByProjectId(request).getTasks();
        if (tasks == null) Assert.assertEquals(0, tasks.size());
        ;
        Assert.assertNotEquals(0, tasks.size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartTaskById() throws SQLException {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = taskList.get(random.nextInt(taskSize)).getId();
        @NotNull final String statusValue = "IN_PROGRESS";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setId(id);
        request.setStatus(status);
        @NotNull final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(request);
        if (response.getTask() == null) return;
        Assert.assertEquals(status, response.getTask().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartTaskByIndex() throws SQLException {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final TaskDTO task = taskList.get(random.nextInt(taskSize));
        @NotNull final Integer index = taskList.indexOf(task);
        @NotNull final String statusValue = "IN_PROGRESS";
        @Nullable final Status status = Status.toStatus(statusValue);
        request.setIndex(index);
        request.setStatus(status);
        @NotNull final TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndex(request);
        if (response.getTask() == null) return;
        Assert.assertEquals(status, response.getTask().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUnbindTaskFromProject() throws Exception {
        @NotNull final TaskUnbindFromProjectRequest requestUnbind = new TaskUnbindFromProjectRequest(token);
        requestUnbind.setProjectId(projectBindID);
        requestUnbind.setTaskId(taskBindID);
        taskEndpoint.unbindTaskFromProject(requestUnbind);

        @NotNull final TaskListByProjectIdRequest requestList = new TaskListByProjectIdRequest(token);
        requestList.setProjectId("02552cef-44f0-4e60-bea6-fd4d94d3bf0a");
        @Nullable final List<TaskDTO> tasks = taskEndpoint.getTaskByProjectId(requestList).getTasks();
        if (tasks == null) {
            return;
        }
        Assert.assertEquals(tasks.size(), 0);
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateTaskById() throws SQLException {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final String id = taskList.get(random.nextInt(taskSize)).getId();
        @NotNull final String name = "new_name";
        @NotNull final String description = "new description";
        request.setId(id);
        request.setName(name);
        request.setDescription(description);
        @Nullable final TaskDTO task = taskEndpoint.updateTaskById(request).getTask();
        if (task == null) {
            return;
        }
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateTaskByIndex() throws SQLException {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final TaskDTO task = taskList.get(random.nextInt(taskSize));
        @NotNull final Integer index = taskList.indexOf(task);
        @NotNull final String name = "new_name";
        @NotNull final String description = "new description";
        request.setIndex(index);
        request.setName(name);
        request.setDescription(description);
        @Nullable final TaskDTO taskUpdated = taskEndpoint.updateTaskByIndex(request).getTask();
        if (taskUpdated == null) {
            return;
        }
        Assert.assertEquals(name, taskUpdated.getName());
        Assert.assertEquals(description, taskUpdated.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveTaskById() throws Exception {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(token);
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskSize);
        @NotNull final String id = taskList.get(index).getId();
        request.setId(id);
        taskEndpoint.removeTaskById(request);
        @NotNull final TaskListRequest requestList = new TaskListRequest(token);
        @NotNull final TaskListResponse responseList = taskEndpoint.listTask(requestList);
        if (responseList.getTasks() == null) return;
        Assert.assertNotEquals(taskSize, responseList.getTasks().size());
        taskList.remove(index);
        taskSize = taskList.size();
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveTaskByIndex() throws Exception {
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(token);
        @NotNull final Random random = new Random();
        @NotNull final TaskDTO task = taskList.get(random.nextInt(taskSize));
        int index = taskList.indexOf(task);
        request.setIndex(index);
        taskEndpoint.removeTaskByIndex(request);
        @NotNull final TaskListRequest requestList = new TaskListRequest(token);
        @NotNull final TaskListResponse responseList = taskEndpoint.listTask(requestList);
        if (responseList.getTasks() == null) return;
        Assert.assertNotEquals(taskSize, responseList.getTasks().size());
        taskList.remove(index);
        taskSize = taskList.size();
    }

    @Test
    @Category(SoapCategory.class)
    public void testClearTasks() throws Exception {
        TaskClearResponse response = taskEndpoint.clearTask(new TaskClearRequest(token));
        Assert.assertNull(response.getTasks());
        taskList.clear();
        createTasks();
    }

}
